import "./App.css";
import data from "./dev-challenge-data.json";
import BarChart from "./BarLineChart";
import { useState } from "react";

const adjustments = [
  { value: "none", label: "Unadjusted" },
  { value: "factor_1", label: "Factor 1" },
  { value: "factor_2", label: "Factor 2" },
];

function App() {
  const [factor, setFactor] = useState("none");

  const updateFactor = ({ target }) => {
    setFactor(target.value);
  };

  return (
    <div className="App">
      <div className="legend">
        <div className="legendItem">
          <div className="blueValue" />
          Blue Value
        </div>
        <div className="legendItem">
          <div className="redValue" />
          Red Value
        </div>
      </div>
      <div className="chart">
        <BarChart
          data={data}
          xValue="Year"
          yBarValue="blue_value"
          yLineValue="red_value"
          factor={factor}
          barTitle="Blue Value"
          lineTitle="Red Value (Millions)"
        />

        <div className="options">
          <p className="optionsTitle">Adjustments</p>
          <div className="adjustmentOptions">
            {adjustments.map(({ value, label }) => (
              <div key={value} className="radioItem">
                <input
                  id={`adjustment-${value}`}
                  type="radio"
                  name="adjustment"
                  value={value}
                  checked={factor === value}
                  onChange={updateFactor}
                />
                <label htmlFor={`adjustment-${value}`}>{label}</label>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
