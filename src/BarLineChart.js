import { useD3 } from "./useD3";
import React from "react";
import * as d3 from "d3";
import "./BarLineChart.css";

function BarLineChart({
  data,
  xValue,
  yBarValue,
  yLineValue,
  factor,
  barTitle,
  lineTitle,
}) {
  const getYBarValue = (d) => {
    const currentFactor = d[factor] || 1;
    return d[yBarValue] / currentFactor;
  };

  const ref = useD3(
    (svg) => {
      const height = 500;
      const width = 1300;
      const margin = { top: 20, right: 30, bottom: 30, left: 40 };

      svg.select(".plot-area").selectAll(".label").remove();

      const x = d3
        .scaleBand()
        .domain(data.map((d) => d[xValue]))
        .rangeRound([margin.left, width - margin.right])
        .padding(0.1);

      const yBar = d3
        .scaleLinear()
        .domain([0, d3.max(data, getYBarValue)])
        .rangeRound([height - margin.bottom, margin.top]);

      const yLine = d3
        .scaleLinear()
        .domain([0, d3.max(data, (d) => d[yLineValue])])
        .rangeRound([height - margin.bottom, margin.top]);

      const line = d3
        .line()
        .x((d) => x(d[xValue]))
        .y((d) => yLine(d[yLineValue]));

      const xAxis = (g) =>
        g.attr("transform", `translate(0,${height - margin.bottom})`).call(
          d3
            .axisBottom(x)
            .tickValues(
              d3
                .ticks(...d3.extent(x.domain()), width / 40)
                .filter((v) => x(v) !== undefined)
            )
            .tickSizeOuter(0)
            .tickFormat((d) => `'${d.toString().substring(2, 4)}`)
        );

      const y1Axis = (g) =>
        g
          .attr("transform", `translate(${margin.left},0)`)
          .call(d3.axisLeft(yBar).ticks(4, "s").tickFormat(d3.format("$,")))
          .call((g) => g.select(".domain").remove())
          .call((g) =>
            g
              .append("text")
              .attr("x", -margin.left)
              .attr("y", 10)
              .attr("fill", "currentColor")
              .attr("text-anchor", "start")
              .text(data.yBar)
          )
          .call((g) =>
            g
              .append("text")
              .attr("x", -margin.left)
              .attr("y", 10)
              .attr("fill", "currentColor")
              .attr("text-anchor", "start")
              .text(barTitle)
          );

      const y2Axis = (g) =>
        g
          .attr("transform", `translate(${width - margin.right},0)`)
          .call(
            d3
              .axisRight(yLine)
              .ticks(4)
              .tickFormat((d) => (d / 1000000).toFixed(1))
          )
          .call((g) => g.select(".domain").remove())
          .call((g) =>
            g
              .append("text")
              .attr("x", margin.right)
              .attr("y", 10)
              .attr("fill", "currentColor")
              .attr("text-anchor", "start")
              .text(data.yLine)
          )
          .call((g) =>
            g
              .append("text")
              .attr("x", -margin.left)
              .attr("y", 10)
              .attr("fill", "currentColor")
              .attr("text-anchor", "start")
              .text(lineTitle)
          );

      const yGuideLines = (g) =>
        g
          .attr("transform", `translate(${margin.left},0)`)
          .call(
            d3
              .axisLeft(yBar)
              .ticks(4)
              .tickSize(-width + margin.left + margin.right)
              .tickFormat("")
          )
          .call((g) => g.select(".domain").remove());

      svg.select(".x-axis").call(xAxis);
      svg.select(".y1-axis").call(y1Axis);
      svg.select(".y2-axis").call(y2Axis);
      svg.select(".guide-lines").call(yGuideLines);

      svg
        .select(".plot-area")
        .selectAll(".bar")
        .data(data)
        .join("rect")
        .attr("class", "bar")
        .attr("x", (d) => x(d[xValue]))
        .attr("width", x.bandwidth())
        .attr("y", (d) => yBar(getYBarValue(d)))
        .attr("height", (d) => yBar(0) - yBar(getYBarValue(d)));
      svg
        .select(".plot-area")
        .selectAll(".text")
        .data(data)
        .enter()
        .append("text")
        .attr("class", "label")
        // 60 is the approximate size of the price labels within the bars, needed to make it center nicely
        .attr(
          "x",
          (d) =>
            -(
              yBar(getYBarValue(d)) +
              (yBar(0) + 60 - yBar(getYBarValue(d))) / 2
            )
        )
        .attr("y", (d) => x(d[xValue]) + x.bandwidth() / 4)
        .attr("dy", ".75em")
        .text((d) => d3.format("$,")(Math.round(getYBarValue(d))));

      svg
        .append("path")
        .attr("class", "line-area")
        .data([data])
        .join("rect")
        .attr("class", "line")
        .attr("d", line);
    },
    [factor]
  );

  return (
    <svg ref={ref} className="svgRoot">
      <g className="legend" />
      <g className="plot-area">
        <g className="guide-lines" />
      </g>
      <g className="x-axis" />
      <g className="y1-axis" />
      <g className="y2-axis" />
    </svg>
  );
}

export default BarLineChart;
